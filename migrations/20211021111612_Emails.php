<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;

class Emails extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();

        $container['db']::schema()->create('emails', function($table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('contact_id');
            $table->string('email');
            $table->smallInteger('status')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();

        $container['db']::schema()->drop('emails');
    }
}
