<?php

declare(strict_types=1);

use Phpmig\Migration\Migration;

class User extends Migration
{
    /**
     * Do the migration
     */
    public function up()
    {
        $container = $this->getContainer();

        $container['db']::schema()->create('users', function($table)
        {
            $table->increments('id');
            $table->string('access_token', 1024);
            $table->string('refresh_token', 1024);
            $table->integer('expires');
            $table->string('base_domain');
            $table->integer('account_id');
            $table->string('access_token_mailchimp')->default('');
            $table->string('server_prefix_mailchimp')->default('');
            $table->smallInteger('mailchimp_status')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
        });
    }

    /**
     * Undo the migration
     */
    public function down()
    {
        $container = $this->getContainer();

        $container['db']::schema()->drop('users');
    }
}
