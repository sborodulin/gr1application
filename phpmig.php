<?php

use Phpmig\Adapter;
use Pimple\Container;
use Illuminate\Database\Capsule\Manager as Capsule;

$container = new Container();

$container['config'] = [
    'driver' => 'mysql',
    'username' => getenv('MYSQL_USER') ?: '',
    'password' => getenv('MYSQL_PASSWORD') ?: '',
    'host' => getenv('MYSQL_HOST') ?: '',
    'database' => getenv('MYSQL_DATABASE') ?: '',
    'port' => getenv('MYSQL_PORT') ?: 3306,
    'charset' => getenv('MYSQL_CHARSET') ?: 'utf8',
    'collation' => getenv('MYSQL_COLLATION') ?: 'utf8_unicode_ci',
    'prefix' => '',
];

$container['db'] = function ($c) {
    $capsule = new Capsule();
    $capsule->addConnection($c['config']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

$container['phpmig.adapter'] = function($c) {
    return new Adapter\Illuminate\Database($c['db'], 'migrations');
};
$container['phpmig.migrations_path'] = __DIR__ . DIRECTORY_SEPARATOR . 'migrations';

return $container;