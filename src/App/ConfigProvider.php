<?php

declare(strict_types=1);

namespace App;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.laminas.dev/laminas-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
            'laminas-cli'  => $this->getCliConfig(),
        ];
    }

    public function getCliConfig(): array
    {
        return [
            'commands' => [
                'app:update-token' => Command\UpdateToken::class,
                'app:worker' => Workers\Work\AccountSyncWorker::class,
                'app:workercorrect' => Workers\Executers\AccountSyncWorker::class,
            ],
        ];
    }

    /**
     * Returns the container dependencies
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                Handler\PingHandler::class => Handler\PingHandler::class,
                Controller\Test::class => Controller\Test::class,
            ],
            'factories'  => [
                Handler\HomePageHandler::class => Handler\HomePageHandlerFactory::class,
                Handler\SumPageHandler::class => Handler\SumPageHandlerFactory::class,
                Handler\MailchimpPageHandler::class => Handler\MailchimpPageHandlerFactory::class,
                Controller\GetMailchimpKey::class => Controller\GetMailchimpKeyFactory::class,
                Command\UpdateToken::class  => Command\UpdateFactory::class,
                Workers\Work\AccountSyncWorker::class => Workers\Work\AccountSyncWorkerFactory::class,
                Workers\Executers\AccountSyncWorker::class => Workers\Executers\AccountSyncWorkerFactory::class,
                Workers\Model\Beanstalk::class => Workers\Model\BeanstalkFactory::class,
                Controller\Webhook::class => Controller\WebhookFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     */
    public function getTemplates(): array
    {
        return [
            'paths' => [
                'app'    => ['templates/app'],
                'error'  => ['templates/error'],
                'layout' => ['templates/layout'],
            ],
        ];
    }
}
