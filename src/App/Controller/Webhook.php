<?php

declare(strict_types=1);

namespace App\Controller;

use App\Workers\Model\Beanstalk;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Webhook implements RequestHandlerInterface
{
    public const QUEUE = 'account_sync';

    protected  Beanstalk $queue;

    public function __construct(Beanstalk $queue)
    {
        $this->queue = $queue;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $params = $request->getParsedBody();

        $accountId = $params['account']['id'];

        if (isset($params['contacts']['add'])) {
            $contact = $params['contacts']['add'][0];
            $contactId = $contact['id'];
            $email = "";
            foreach ($contact['custom_fields'] as $key => $val) {
                if ($val['code'] === "EMAIL") {
                    $email = $val['values'][0]['value'];
                    break;
                }
            }
            if (isset($accountId) && isset($contactId) && $email !== "")
                $this->queue->setTask('addEmail', static::QUEUE, null, $accountId, $contactId, $email);
        }
        if (isset($params['contacts']['delete'])) {
            $contact = $params['contacts']['delete'][0];
            $contactId = $contact['id'];
            if (isset($accountId) && isset($contactId))
                $this->queue->setTask('deleteEmail', static::QUEUE, null, $accountId, $contactId);
        }
        if (isset($params['contacts']['update'])) {
            $contact = $params['contacts']['update'][0];
            $contactId = $contact['id'];
            $email = "";
            foreach ($contact['custom_fields'] as $key => $val) {
                if ($val['code'] === "EMAIL") {
                    $email = $val['values'][0]['value'];
                    break;
                }
            }
            if (isset($accountId) && isset($contactId) && $email !== "")
                $this->queue->setTask('updateEmail', static::QUEUE, null, $accountId, $contactId, $email);
        }
        return new JsonResponse($params);
    }
}
