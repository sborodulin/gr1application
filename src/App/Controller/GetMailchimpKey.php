<?php

declare(strict_types=1);

namespace App\Controller;

use App\Handler\MailchimpPageHandler;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use MailchimpMarketing;
use App\Models\User;

class GetMailchimpKey implements RequestHandlerInterface
{
    private array $configKey;

    protected MailchimpPageHandler $mailchimpPageHandler;

    public function __construct(array $configKey, MailchimpPageHandler $mailchimpPageHandler) {
        $this->configKey = $configKey;

        $this->mailchimpPageHandler = $mailchimpPageHandler;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $params = $request->getQueryParams();

        $mailchimpClientId = $this->configKey['mailchimp_client_id'];
        $mailchimpClientSecret = $this->configKey['mailchimp_client_secret'];
        $baseUrl = $this->configKey['base_url'];
        $oauthCallback = "$baseUrl/api/v1/mailchimpkey";

        if (isset($params['login']) && isset($params['accountid'])) {
            header('Location: https://login.mailchimp.com/oauth2/authorize?' . http_build_query([
                    'response_type' => 'code',
                    'client_id' => $mailchimpClientId,
                    'redirect_uri' => $oauthCallback,
                    'state' => $params['accountid'],
                ]));
            exit();
        }

        if (isset($params['code']) && isset($params['state'])) {
            $url = 'https://login.mailchimp.com/oauth2/token';
            $context = stream_context_create([
                'http' => [
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query([
                        'grant_type' => "authorization_code",
                        'client_id' => $mailchimpClientId,
                        'client_secret' => $mailchimpClientSecret,
                        'redirect_uri' => $oauthCallback,
                        'code' => $params['code'],
                    ]),
                ],
            ]);
            $result = file_get_contents($url, false, $context);
            $decoded = json_decode($result);
            $accessToken = $decoded->access_token;

            $url = 'https://login.mailchimp.com/oauth2/metadata';
            $context = stream_context_create([
                'http' => [
                    'header' => "Authorization: OAuth $accessToken",
                ],
            ]);
            $result = file_get_contents($url, false, $context);
            $decoded = json_decode($result);
            $dc = $decoded->dc;

            $mailchimp = new MailchimpMarketing\ApiClient();
            $mailchimp->setConfig([
                'accessToken' => $accessToken,
                'server' => $dc,
            ]);
            try {
                $response = $mailchimp->ping->get();
                User::where('account_id', $params['state'])
                    ->update([  'access_token_mailchimp' => $accessToken,
                                'server_prefix_mailchimp' => $dc,
                                'mailchimp_status' => 1,
                            ]);
                $this->mailchimpPageHandler->actionContacts((int)$params['state']);
                return new JsonResponse("Access token is $accessToken and their server prefix is $dc . " . json_encode($response));
            } catch (Exception $e) {
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        }
        return new JsonResponse($this->configKey);
    }
}
