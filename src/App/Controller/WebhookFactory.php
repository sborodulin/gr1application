<?php
 
declare(strict_types=1);
 
namespace App\Controller;

use App\Workers\Model\Beanstalk;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
 
class WebhookFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        return new Webhook($container->get(Beanstalk::class));
    }
}
