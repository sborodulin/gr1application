<?php
 
declare(strict_types=1);
 
namespace App\Controller;

use App\Handler\MailchimpPageHandler;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
 
class GetMailchimpKeyFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $configMailChimpKey = $container->get('config')['mailchimpkey'];

        return new GetMailchimpKey($configMailChimpKey, $container->get(MailchimpPageHandler::class));
    }
}
