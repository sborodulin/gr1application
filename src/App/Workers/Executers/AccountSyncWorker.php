<?php

declare(strict_types=1);

namespace App\Workers\Executers;

use AmoCRM\Exceptions\AmoCRMApiException;
use App\Handler\MailchimpPageHandler;
use App\Models\User;
use App\Workers\Model\Beanstalk;
use MailchimpMarketing;
use App\Models\Emails;
use AmoCRM\Client\AmoCRMApiClient;
use App\Helpers\GetApiClient;

class AccountSyncWorker extends BeanstalkWorker
{

    public const NAME = 'crm:worker:account_sync';
    public const QUEUE = 'account_sync';
    protected AmoCRMApiClient $apiClient;
    protected MailchimpPageHandler $mailchimpPageHandler;
    protected string $listId;

    public function __construct(Beanstalk $queue, AmoCRMApiClient $apiClient, MailchimpPageHandler $mailchimpPageHandler, array $config)
    {
        parent::__construct($queue);

        $this->apiClient = $apiClient;
        $this->mailchimpPageHandler = $mailchimpPageHandler;
        $this->listId = $config['listId'];
    }

    protected function myName(): string
    {
        return self::NAME;
    }

    protected function process(array $job): void
    {
        $action   = $job['action'];
        if (isset($job['data'])) {
            $data = $job['data'];
        } else {
            $data = [];
        }
        if (method_exists($this, $action)) {
            $this->$action($data);
        } else {
            echo "action not found\n";
        }
    }

    public function getContacts(array $data): void
    {
        $user = $data['user'];

        $apiClientClass = new GetApiClient($user, $this->apiClient, $this->mailchimpPageHandler);
        $apiClient = $apiClientClass->getApiClient();

        $contacts = $apiClient->contacts()->get()->all();
        foreach ($contacts as $contObj) {
            //Получим один контакт
            try {
                $contact = $this->apiClient->contacts()->getOne($contObj->id);
            } catch (AmoCRMApiException $e) {
                throw \Exception($e->getMessage(), $e->getCode());
            }

            //Получим коллекцию значений полей контакта
            $customFields = $contact->getCustomFieldsValues();
            //Получим значение поля по его коду
            $emailField = $customFields->getBy('fieldCode', 'EMAIL');
            if (!empty($emailField)) {
                $email = $emailField->toArray()['values'][0]['value'];
                $user = User::query()->where("account_id", "=", $user['account_id'])->first();
                Emails::updateOrCreate(
                    [
                        'user_id' => $user['id'],
                        'contact_id' => $contObj->id,
                        'email' => $email,
                    ]
                );
            }
        }
    }

    public function sentEmails(array $data): void
    {
        $user = $data['user'];

        $emails = Emails::where("user_id", $user['id'])->get();

        foreach ($emails as $email) {
            if ($email->getAttributeValue('status') === 0) {
                $email = $email->getAttributeValue('email');
                if ($user['mailchimp_status'] == 0)
                    throw \Exception("Don\'t get mailchimp key", 103);
                $apiKey = $user['access_token_mailchimp'];
                $serverPrefix = $user['server_prefix_mailchimp'];

                $mailchimp = new \MailchimpMarketing\ApiClient();
                $mailchimp->setConfig([
                    'apiKey' => $apiKey,
                    'server' => $serverPrefix
                ]);

                try {
                    $response = $mailchimp->lists->addListMember($this->listId, [
                        "email_address" => $email,
                        "status" => "subscribed",
                        "merge_fields" => [
                            "FNAME" => "Test",
                            "LNAME" => "Testov"
                        ]
                    ]);
                    Emails::updateOrCreate(
                        [
                            'user_id' => $user['id'],
                            'email' => $email,
                        ],
                        [
                            'status' => 1,
                        ]
                    );
                } catch (MailchimpMarketing\ApiException $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    public function initializer(array $data): array
    {
        $accountId = $data['accountId'];

        $user = User::query()->where("account_id", "=", $accountId)->first();

        if ($user['mailchimp_status'] == 0)
            throw \Exception("Don\'t get mailchimp key", 103);
        $apiKey = $user['access_token_mailchimp'];
        $serverPrefix = $user['server_prefix_mailchimp'];

        $mailchimp = new \MailchimpMarketing\ApiClient();
        $mailchimp->setConfig([
            'apiKey' => $apiKey,
            'server' => $serverPrefix
        ]);
        return ['mailchimp' => $mailchimp, 'user' => $user];
    }

    public function addEmail(array $data): void
    {
        $result = $this->initializer($data);
        $mailchimp = $result['mailchimp'];
        $user = $result['user'];

        try {
            $response = $mailchimp->lists->addListMember($this->listId, [
                "email_address" => $data['email'],
                "status" => "subscribed",
                "merge_fields" => [
                    "FNAME" => "Test",
                    "LNAME" => "Testov"
                ]
            ]);

            Emails::updateOrCreate(
                [
                    'user_id' => $user['id'],
                    'contact_id' => $data['contactId'],
                    'email' => $data['email'],
                ],
                [
                    'status' => 1,
                ]
            );
        } catch (MailchimpMarketing\ApiException $e) {
            echo $e->getMessage();
        }
    }

    public function deleteEmail(array $data): void
    {
        $result = $this->initializer($data);
        $mailchimp = $result['mailchimp'];
        $user = $result['user'];

        $email = Emails::query()->where('user_id', '=', $user['id'])
                       ->where('contact_id','=', $data['contactId'])
                       ->first();

        $emailValue = $email['email'];
        $subscriberHash = md5(strtolower($emailValue));

        try {
            $response = $mailchimp->lists->deleteListMember($this->listId, $subscriberHash);

            $email->delete();
        } catch (MailchimpMarketing\ApiException $e) {
            echo $e->getMessage();
        }
    }

    public function updateEmail(array $data): void
    {
        $result = $this->initializer($data);
        $mailchimp = $result['mailchimp'];
        $user = $result['user'];

        $email = Emails::query()->where('user_id', '=', $user['id'])
            ->where('contact_id','=', $data['contactId'])
            ->first();

        $emailForReplacement = $email['email'];
        $subscriberHash = md5(strtolower($emailForReplacement));

        try {
            $response = $mailchimp->lists->updateListMember($this->listId, $subscriberHash, [
                "email_address" => $data['email'],
                "status" => "subscribed",
                "merge_fields" => [
                    "FNAME" => "Test",
                    "LNAME" => "Testov"
                ]
            ]);

            Emails::updateOrCreate(
                [
                    'user_id' => $user['id'],
                    'contact_id' => $data['contactId'],
                ],
                [
                    'email' => $data['email'],
                    'status' => 1,
                ]
            );
        } catch (MailchimpMarketing\ApiException $e) {
            echo $e->getMessage();
        }
    }
}
