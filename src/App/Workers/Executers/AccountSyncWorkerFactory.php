<?php

declare(strict_types=1);

namespace App\Workers\Executers;

use AmoCRM\Client\AmoCRMApiClient;
use App\Handler\MailchimpPageHandler;
use App\Workers\Model\Beanstalk;
use Psr\Container\ContainerInterface;


class AccountSyncWorkerFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['mailchimp'];
        $configMailchimp = $container->get('config')['mailchimpkey'];

        $apiClient = new AmoCRMApiClient(
            $config["clientId"],
            $config["clientSecret"],
            $config["redirectUri"],
        );

        return new AccountSyncWorker($container->get(Beanstalk::class), $apiClient, $container->get(MailchimpPageHandler::class), $configMailchimp);
    }
}
