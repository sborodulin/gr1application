<?php

declare(strict_types=1);

namespace App\Workers\Model;

use Pheanstalk\Pheanstalk;

class Beanstalk
{
    protected Pheanstalk $connect;

    protected array $config;

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function setTask(string $methodName, string $tube, $user, string $accountId = "", string $contactId = "", string $email = ""): Pheanstalk
    {
        $connect = $this->getConnect();

        $job = [
            "action" => $methodName,
            "data" => [
                "user" => $user,
                "accountId" => $accountId,
                "contactId" => $contactId,
                "email" => $email,
            ],
        ];
        $connect->useTube($tube)->put(json_encode($job));

        return $connect;
    }

    public function getConnect(): Pheanstalk
    {
        $this->connect = Pheanstalk::create(
            $this->config['host'],
            $this->config['port'],
            $this->config['timeout'],
        );

        return $this->connect;
    }
}
