<?php

declare(strict_types=1);

namespace App\Workers\Model;

use Psr\Container\ContainerInterface;

class BeanstalkFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['beanstalkd'];

        return new Beanstalk($config);
    }
}
