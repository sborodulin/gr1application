<?php

declare(strict_types=1);

namespace App\Handler;

use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use League\OAuth2\Client\Grant\AuthorizationCode;
use App\Models\User;
use App\Workers\Model\Beanstalk;
use AmoCRM\Client\AmoCRMApiClient;

class MailchimpPageHandler implements RequestHandlerInterface
{
    public const QUEUE = 'account_sync';

    protected AmoCrm $provider;

    protected  Beanstalk $queue;

    private string $baseUrlMailchimp;

    protected AmoCRMApiClient $apiClient;

    public function __construct(AmoCrm $provider, string $baseUrlMailchimp, Beanstalk $queue, AmoCRMApiClient $apiClient)
    {
        $this->provider = $provider;
        $this->baseUrlMailchimp = $baseUrlMailchimp;
        $this->queue = $queue;
        $this->apiClient = $apiClient;
    }

    private function addMailchimpToken($accountId): void
    {
        $mailchimpKeyStatus = User::where('account_id', $accountId)->get();
        if (!$mailchimpKeyStatus[0]->mailchimp_status) {
            header("Location: $this->baseUrlMailchimp/api/v1/mailchimpkey?login&accountid=".$mailchimpKeyStatus[0]->account_id);
            exit( );
        }
    }

    public function saveToken($accessToken, $actionContacts = false): void
    {
        if (
            isset($accessToken)
            && isset($accessToken['accessToken'])
            && isset($accessToken['refreshToken'])
            && isset($accessToken['expires'])
            && isset($accessToken['baseDomain'])
            && isset($accessToken['accountId'])
        ) {
            $data = [
                'accessToken' => $accessToken['accessToken'],
                'expires' => $accessToken['expires'],
                'refreshToken' => $accessToken['refreshToken'],
                'baseDomain' => $accessToken['baseDomain'],
                'accountId' => $accessToken['accountId'],
            ];

            $user = User::updateOrCreate(
                [
                    'account_id' => $data['accountId'],
                    ],
                [
                    'access_token' => $data['accessToken'],
                    'refresh_token' => $data['refreshToken'],
                    'expires' => $data['expires'],
                    'base_domain' => $data['baseDomain'],
                    ]
            );

            if (!$actionContacts) {
                $this->addMailchimpToken($accessToken['accountId']);
                $this->actionContacts((int)$accessToken['accountId']);
            }
        } else {
            throw new \Exception('Invalid access token ' . $accessToken, 102);
        }
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $params = $request->getQueryParams();

        $result = [
            'error' => [],
        ];
        if (!isset($params["referer"])) {
            $result['error'][] = 'referer';
        }
        if (!isset($params['client_id'])) {
            $result['error'][] = 'client_id';
        }
        if (!isset($params['code'])) {
            $result['error'][] = 'code';
        }
        if (count($result['error']) > 0) {
            throw new \Exception(json_encode($result), 101);
        }

        $this->provider->setBaseDomain($params['referer']);

        /**
         * Ловим обратный код
         */
        try {
            /** @var \League\OAuth2\Client\Token\AccessToken $access_token */
            $accessToken = $this->provider->getAccessToken(new AuthorizationCode(), [
                'code' => $params['code'],
            ]);

            if (!$accessToken->hasExpired()) {
                $this->saveToken([
                    'accessToken' => $accessToken->getToken(),
                    'refreshToken' => $accessToken->getRefreshToken(),
                    'expires' => $accessToken->getExpires(),
                    'baseDomain' => $this->provider->getBaseDomain(),
                    'accountId' => $this->apiClient->getOAuthClient()->getAccountDomain($accessToken)->getId(),
                ]);
            }
        } catch (Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        /** @var \AmoCRM\OAuth2\Client\Provider\AmoCRMResourceOwner $ownerDetails */
        $ownerDetails = $this->provider->getResourceOwner($accessToken);

        return new JsonResponse('Hello, ' . $ownerDetails->getName() . '!');
    }

    public function actionContacts(int $accountId): void
    {
        $this->getContacts($accountId);
        $this->sentContacts($accountId);
    }

    public function getContacts(int $accountId): void
    {
        $user = User::query()->where("account_id","=", $accountId)->first();

        $this->queue->setTask('getContacts', static::QUEUE, $user);
    }

    public function sentContacts(int $accountId): void
    {
        $user = User::query()->where("account_id","=", $accountId)->first();

        $this->queue->setTask('sentEmails', static::QUEUE, $user);
    }
}
