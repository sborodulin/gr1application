<?php
 
declare(strict_types=1);
 
namespace App\Handler;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use App\Workers\Model\Beanstalk;
use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
 
class MailchimpPageHandlerFactory
{
    public function __invoke(ContainerInterface $container): RequestHandlerInterface
    {
        $config = $container->get('config')['mailchimp'];
        $configMailChimpKey = $container->get('config')['mailchimpkey'];

        $provider = new AmoCRM([
            'clientId' => $config["clientId"],
            'clientSecret' => $config["clientSecret"],
            'redirectUri' => $config["redirectUri"],
        ]);

        $apiClient = new AmoCRMApiClient(
            $config["clientId"],
            $config["clientSecret"],
            $config["redirectUri"],
        );

        return new MailchimpPageHandler($provider, $configMailChimpKey['base_url'], $container->get(Beanstalk::class), $apiClient);
    }
}
