<?php
 
declare(strict_types=1);
 
namespace App\Handler;
 
use Laminas\Diactoros\Response\HtmlResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
 
class SumPageHandler implements RequestHandlerInterface
{
  
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $numbers = $request->getQueryParams();
        if (count($numbers) === 0) {
        	return new HtmlResponse(sprintf(
            	'<h1>Параметры не переданы</h1>'
        	));
        }
        if (count($numbers) > 10) {
        	return new HtmlResponse(sprintf(
            	'<h1>Передано больше 10 параметеров</h1>'
        	));
        }        
        
        $sum = 0;
        $badNumbers = [];
        foreach($numbers as $key => $number) {
        	$number = htmlspecialchars($number, ENT_HTML5, 'UTF-8');
        	if(!is_numeric($number)) {
        		$badNumbers += [$key => $number];
        	} else {
        		$sum += $number;
        	}
        }

        if(count($badNumbers) !== 0) {
        	$badNumbersString = "";
		foreach($badNumbers as $key => $val) {
			$badNumbersString .= $key." => ".$val."<br>";
		}
                return new HtmlResponse(sprintf(
		    	'<h1>Ошибка в переданных параметрах<br>%s</h1>',
		   	$badNumbersString
        	));
        }
        
        return new HtmlResponse(sprintf(
            	'<h1>Сумма равна %f</h1>',
            	$sum
        	));
         
    }
}
