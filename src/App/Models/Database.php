<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Capsule\Manager as Capsule;

class Database {

    private $configDB;

    function __construct($configDB) {
        $this->configDB = $configDB;

        $capsule = new Capsule;
        $capsule->addConnection($this->configDB);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
