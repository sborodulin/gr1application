<?php

declare(strict_types=1);

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class Emails extends Model {
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'emails';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'contact_id',
        'email',
        'status',
    ];
}
