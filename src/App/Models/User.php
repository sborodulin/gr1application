<?php

namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class User extends Model {
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = ['access_token',
                            'refresh_token',
                            'expires',
                            'base_domain',
                            'account_id',
                            'access_token_mailchimp',
                            'server_prefix_mailchimp',
                            'mailchimp_status',
    ];
}
