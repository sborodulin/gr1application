<?php

namespace App\Helpers;

use AmoCRM\Client\AmoCRMApiClient;
use League\OAuth2\Client\Token\AccessToken;
use App\Handler\MailchimpPageHandler;

class GetApiClient
{
    protected array $user;
    protected AmoCRMApiClient $apiClient;
    protected MailchimpPageHandler $mailchimpPageHandler;

    public function __construct(array $user, AmoCRMApiClient $apiClient, MailchimpPageHandler $mailchimpPageHandler)
    {
        $this->user = $user;
        $this->apiClient = $apiClient;
        $this->mailchimpPageHandler = $mailchimpPageHandler;
    }

    public function getApiClient(): AmoCRMApiClient
    {
        $accessToken = new AccessToken([
            'access_token' => $this->user['access_token'],
            'refresh_token' => $this->user['refresh_token'],
            'expires' => $this->user['expires'],
            'baseDomain' => $this->user['base_domain']
        ]);

        $this->apiClient->setAccessToken($accessToken)
            ->setAccountBaseDomain($accessToken->getValues()['baseDomain'])
            ->onAccessTokenRefresh(
                function ($user, $accessToken) {
                    $this->mailchimpPageHandler->saveToken(
                        [
                            'accessToken' => $user->getAttributeValue('access_token'),
                            'refreshToken' => $user->getAttributeValue('refresh_token'),
                            'expires' => $user->getAttributeValue('expires'),
                            'baseDomain' => $user->getAttributeValue('base_domain'),
                            'accountId' => $this->apiClient->getOAuthClient()->getAccountDomain($accessToken)->getId(),
                        ],
                        true
                    );
                }
            );
        return $this->apiClient;
    }
}
