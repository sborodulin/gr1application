<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use League\OAuth2\Client\Grant\RefreshToken;
use App\Models\User;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use AmoCRM\Client\AmoCRMApiClient;

use function sprintf;

final class UpdateToken extends Command
{
    private AmoCrm $provider;
    private AmoCRMApiClient $apiClient;

    public function __construct(AmoCrm $provider, AmoCRMApiClient $apiClient) {
        parent::__construct();

        $this->provider = $provider;
        $this->apiClient = $apiClient;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach (User::all() as $key => $val) {
            /**
             * Получаем токен по рефрешу
             */
            try {
                $accessToken = $this->provider->getAccessToken(new RefreshToken(), [
                    'refresh_token' => $val->getAttributeValue('refresh_token'),
                ]);

                $data = [];
                if (!$accessToken->hasExpired()) {
                    $data['accessToken'] = $accessToken->getToken();
                    $data['refreshToken'] = $accessToken->getRefreshToken();
                    $data['expires'] = $accessToken->getExpires();
                    $data['baseDomain'] = $this->apiClient->getOAuthClient()->getAccountDomain($accessToken)->getDomain();
                    $data['accountId'] = $this->apiClient->getOAuthClient()->getAccountDomain($accessToken)->getId();
                }

                //сохраняем новые access, refresh токены и привязку к аккаунту и возможно пользователю
                if (
                    isset($data)
                    && isset($data['accessToken'])
                    && isset($data['refreshToken'])
                    && isset($data['expires'])
                    && isset($data['baseDomain'])
                ) {
                    User::updateOrCreate(
                        [
                            'account_id' => $data['accountId'],
                        ],
                        [
                            'access_token' => $data['accessToken'],
                            'refresh_token' => $data['refreshToken'],
                            'expires' => $data['expires'],
                            'base_domain' => $data['baseDomain'],
                        ]
                    );
                } else {
                    throw new \Exception('Invalid access token ' . $accessToken, 103);
                }
            } catch (Exception $e) {
                throw new \Exception($e->getMessage(), $e->getCode());
            }
        }

        $output->writeln(sprintf('<info>All Ok<info>! '));

        return 0;
    }
}
