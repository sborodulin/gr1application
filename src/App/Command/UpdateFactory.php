<?php

declare(strict_types=1);

namespace App\Command;

use AmoCRM\Client\AmoCRMApiClient;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use Psr\Container\ContainerInterface;

class UpdateFactory
{
    public function __invoke(ContainerInterface $container)
    {
        $config = $container->get('config')['mailchimp'];

        $provider = new AmoCRM([
            'clientId' => $config['clientId'],
            'clientSecret' => $config['clientSecret'],
            'redirectUri' => $config['redirectUri'],
        ]);

        $apiClient = new AmoCRMApiClient(
            $config["clientId"],
            $config["clientSecret"],
            $config["redirectUri"],
        );

        return new UpdateToken($provider, $apiClient);
    }
}
